#include "widget.h"
#include "ui_widget.h"
#include <QMessageBox>

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    change_money(0);

}

Widget::~Widget()
{
    delete ui;
}



void Widget::change_money(int diff){

    money+=diff;
    ui->lcdNumber->display(money);
    if(money==0) ui->pbReset->setEnabled(false);
    else ui->pbReset->setEnabled(true);
    if(money>=100) ui->pbCoffee->setEnabled(true);
    else ui->pbCoffee->setEnabled(false);
    if(money>=150) ui->pbTea->setEnabled(true);
    else ui->pbTea->setEnabled(false);
    if(money>=200) ui->pbMilk->setEnabled(true);
    else ui->pbMilk->setEnabled(false);




}

void Widget::on_pbCoin10_clicked()
{
    change_money(10);
}

void Widget::on_pbCoin50_clicked()
{
    change_money(50);
}

void Widget::on_pbCoin100_clicked()
{
    change_money(100);
}

void Widget::on_pbCoin500_clicked()
{
    change_money(500);
}

void Widget::on_pbCoffee_clicked()
{
    change_money(-100);
}

void Widget::on_pbTea_clicked()
{
    change_money(-150);
}

void Widget::on_pbMilk_clicked()
{
    change_money(-200);
}

void Widget::on_pbReset_clicked()
{
    char buf [128];
    QMessageBox mb;
    int money_500 = money/500;
    money -= money_500*500;
    int money_100 = money/100;
    money -= money_100*100;
    int money_50 = money/50;
    money -= money_50*50;
    int money_10 = money/10;
    money -= money_10*10;
    sprintf (buf, "500 : %d\n100 : %d\n50 : %d\n10 : %d", money_500, money_100, money_50, money_10);
    mb.information(this,"Remained Money",buf);
    change_money(-money);
    ui->lcdNumber->display(money);


}
